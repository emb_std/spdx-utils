#!/bin/bash

# SPDX-FileCopyrightText: 2021-2022 embeach
# 
# SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
# 
# Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
# Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
# Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
# 
# Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/spdx-utils.git@master#spdx-utils-main/make-helper.sh
# Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/spdx-utils.git@master#spdx-utils-main/make-helper.sh
# Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/template-ca-lr.git@master#spdx-utils/make-helper.sh
# 
# Note-PCRLT:emb.7:specify-identity-name:embeach
# Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/spdx-utils.git@master#spdx-utils-main/make-helper.sh
# Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding spdx-utils-main/make-helper.sh
# 
# Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

#make-helper for Linux only! No warranty!
#make-helper.sh current working dir is expected to be spdx-utils-main
SPDX_UTILS_MAIN=.
MINICONDA_ROOT=$HOME/miniconda3

CONFIG_PATH1=config #default values and target repository path
. $CONFIG_PATH1
CONFIG_PATH2=$TARGET_REPOSITORY_PATH/spdx-utils-input/config #target repository specific values
if [ -f $CONFIG_PATH2 ]; then
	. $CONFIG_PATH2
fi
if [ $1 = 'forFsfeReuseTool_download-and-install-and-init-miniconda3' ]
then
	mkdir -p $HOME/Downloads
	cd $HOME/Downloads
	#download miniconda3
	echo 'wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh..'
	wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
	#install miniconda3 in $HOME/miniconda3 with default settings
	echo 'bash $HOME/Downloads/Miniconda3-latest-Linux-x86_64.sh -b -p '$MINICONDA_ROOT
	bash $HOME/Downloads/Miniconda3-latest-Linux-x86_64.sh -b -p $MINICONDA_ROOT
	#initialize miniconda3 - will add an export-conda-path-command to .bashrc and somehow seems to be usable to activate environments
	echo $MINICONDA_ROOT'/bin/conda init..'
	$MINICONDA_ROOT/bin/conda init
	#after init, shell has to be restarted for changes in .bashrc to take effect..
elif [ $1 = 'forFsfeReuseTool_create-conda-env' ]
then
	#the hook seems to be needed to prevent warning of missing conda init
	eval "$(conda shell.bash hook)"
	#(echo y) | conda create -p ./.cenv python=3.9.1 pip=21.0.1
	(echo y) | conda create -p $SPDX_UTILS_MAIN/.cenv --file $SPDX_UTILS_MAIN/requirements_conda_base.txt
elif [ $1 = 'forFsfeReuseTool_install-deps-in-conda-env' ]
then
	eval "$(conda shell.bash hook)"
	conda activate $SPDX_UTILS_MAIN/.cenv/
	which python
	#pip install reuse
	pip install -r $SPDX_UTILS_MAIN/requirements_pip.txt
elif [ $1 = 'reuseTool_lint' ]
then
	eval "$(conda shell.bash hook)"
	conda activate $SPDX_UTILS_MAIN/.cenv/
	which python
	echo "TARGET_REPOSITORY_PATH=$TARGET_REPOSITORY_PATH (absolut or relative to spdx-utils-main)"
	reuse lint > $TARGET_REPOSITORY_PATH/spdx-utils-output/reuse.txt
	cat $TARGET_REPOSITORY_PATH/spdx-utils-output/reuse.txt
elif [ $1 = 'reuseTool_spdx' ]
then
	eval "$(conda shell.bash hook)"
	conda activate $SPDX_UTILS_MAIN/.cenv/
	which python
	echo "TARGET_REPOSITORY_PATH=$TARGET_REPOSITORY_PATH (absolut or relative to spdx-utils-main)"
	reuse spdx > $TARGET_REPOSITORY_PATH/spdx-utils-output/reuse.spdx
elif [ $1 = 'pcrltTool_generateSpdx' ]
then
	echo "TARGET_REPOSITORY_PATH=$TARGET_REPOSITORY_PATH (absolut or relative to spdx-utils-main)"
	echo "MAX_FILES=$MAX_FILES"
	echo "TARGET_PACKAGE_NAME=$TARGET_PACKAGE_NAME"
	echo "TARGET_PACKAGE_VERSION=$TARGET_PACKAGE_VERSION"
	echo "SPDX_CREATOR_PERSON=$SPDX_CREATOR_PERSON"
	echo "OTHER_CMD_OPTIONS=$OTHER_CMD_OPTIONS"
	java -jar pcrlt-validator/target/pcrlt-validator.jar -a -m=$MAX_FILES "$TARGET_REPOSITORY_PATH" --package-name=$TARGET_PACKAGE_NAME --package-version=$TARGET_PACKAGE_VERSION --spdx-creator-person="$SPDX_CREATOR_PERSON" $OTHER_CMD_OPTIONS
	tail -3 $TARGET_REPOSITORY_PATH/spdx-utils-output/pcrlt.txt
elif [ $1 = 'remove_generatedSpdxFiles' ]
then
	echo "TARGET_REPOSITORY_PATH=$TARGET_REPOSITORY_PATH (absolut or relative to spdx-utils-main)"
	rm -f $TARGET_REPOSITORY_PATH/spdx-utils-output/pcrlt.spdx
	rm -f $TARGET_REPOSITORY_PATH/spdx-utils-output/reuse.spdx
else
	echo "unknown argument"
fi
