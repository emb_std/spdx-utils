<!--
SPDX-FileCopyrightText: 2021-2022 embeach

SPDX-License-Identifier: MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later

Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/spdx-utils.git@master#spdx-utils-main/README_SPDX_UTILS.md
Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/template-ca-lr.git@master#spdx-utils/README_REUSE_tool.md
Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/template-ca-lr.git@master#spdx-utils/README_SPDX.md

Note-PCRLT:emb.7:specify-identity-name:embeach
Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/spdx-utils.git@master#spdx-utils-main/README_SPDX_UTILS.md
Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding README_SPDX_UTILS.md

Note-PCRLT:MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"
-->
# SPDX Utils Main
This folder contains bootstrap instructions and scripts to use some tools to validate compliance with REUSE[^reuse] and PCLRT[^pcrlt] as well as to generate SPDX[^spdx] documents.  
Currently two tools are integrated.
1. fsfe reuse-tool <https://git.fsfe.org/reuse/tool> the following modified version is used: <https://gitlab.com/embeach/reuse-tool> (status: rather stable, features: check REUSE compliance, generate SPDX documents)
2. pcrlt-validator <https://gitlab.com/emb_std/pcrlt-validator> (status: rapid-prototype, features: check PCRLT compliance, generate SPDX documents, generate PCRLT reports)

## Expected bootstrapping
The `target-repository` is the repository which should be analysed towards REUSE or PCRLT compliance. It can have any name (e.g. `spdx-utils` in case of the spdx-utils repository).  
The recommended setup is to have a `Makefile` in the `target-repository` with the make target `download-spdx-utils-main` - the following lines can be used for this (copied from git+https://gitlab.com/emb_std/spdx-utils.git#Makefile):
```Makefile
download-spdx-utils-main:
	if [ -d "spdx-utils-main" ]; then echo "The folder already exists. A download does not seem to be necessary." && false ; fi
	git clone https://gitlab.com/emb_std/spdx-utils.git tmp_spdx-utils
	cd tmp_spdx-utils && git checkout tags/v0.1.4
	cp -a tmp_spdx-utils/spdx-utils-main spdx-utils-main
	cp tmp_spdx-utils/spdx-utils-input/config spdx-utils-main/spdx-utils.version
	rm -rf tmp_spdx-utils
```

After executing the following, the `spdx-utils-main` folder (from `https://gitlab.com/emb_std/spdx-utils`) should be present at `target-repository/spdx-utils-main`:
```console
user@host:~/some_path$ git clone https://path-to-target-repository/target-repository.git
user@host:~/some_path$ cd target-repository
user@host:~/some_path/target-repository$ make download-spdx-utils-main
```
The path to the `target-repository` can be specified in `spdx-utils-main/config`. This can be absolut or relative to `spdx-utils-main`. The default value is `..`. If the `target-repository` root is not in the parent folder of `spdx-utils-main`, it needs to be changed.  
The `target-repository` needs to meet the following conditions:
1. It contains `/.tmpCopyFilesForVerificationCode/` in the `target-repository` root .gitignore
2. It contains `/spdx-utils-main/` in the `target-repository` root .gitignore (if the default configuration is used)
3. It contains a file `target-repository/spdx-utils-input/spdx_document_template.txt` (can be empty)
4. It contains a folder `target-repository/spdx-utils-output` (should be empty, as contents might be overwritten)
5. (the repository is backed up, because the tools will write in this folder)

The <https://gitlab.com/emb_std/pcrlt-validator> repository is an example that meets these conditions.

## REUSE tool
REUSE <https://reuse.software> is a standard and recommended way to specify copyright and license information per file. To check if a repository is REUSE compliant, the fsfe REUSE tool can be used. For this you can use the setup instructions from <https://git.fsfe.org/reuse/tool> or you can alternatively use the provided Makefile.

### Setup miniconda3, python, pip and REUSE tool by using the Makefile (Linux only!)
The following was tested on debian buster. (requires: git, bash, make)  
  
Make sure that the `spdx-utils-main` folder is present in `~/some_path/target-repository/spdx-utils-main` (see section "Expected bootstrapping") and navigate there:
```console
user@host:~/some_path/target-repository$ cd spdx-utils-main
```
The following will download and install miniconda3 in `$HOME/miniconda3`:
```console
user@host:~/some_path/target-repository/spdx-utils-main$ make forFsfeReuseTool_0download-and-install-and-init-miniconda3
```
After this, the shell has to be restarted.  
To setup a python environment (in `~/some_path/target-repository/spdx-utils-main/.cenv`) and install pip dependencies run:
```console
user@host:~/some_path/target-repository/spdx-utils-main$ make forReuseTool_1create-conda-env
user@host:~/some_path/target-repository/spdx-utils-main$ make forReuseTool_2install-deps-in-conda-env
```
To create a REUSE compliance report ( `reuse.txt` ) and a SPDX document ( `reuse.spdx` ) in the spdx-utils-output folder ( `~/some_path/target-repository/spdx-utils-output` ), run:  

```console
user@host:~/some_path/target-repository/spdx-utils-main$ make reuseTool_all
```

For further details see "Further Notes"

## PCRLT tool (pcrlt-validator)
PCRLT <https://gitlab.com/emb_std/pcrlt> (Privacy-aware Content, Rights Holder and License Tracing) is a proposed standard or recommendation to specify additional information in addition to the REUSE standard or recommendations. To check if a repository is PCRLT compliant (and to which degree), the experimental PCRLT tool (pcrlt-validator) can be used. The provided Makefile can be used to bootstrap the tool chain.  

### Setup pcrlt-validator (Linux only!)
The following was tested on debian buster. (requires: maven, java8+, git, bash, make)  
The following will download and install the PCRLT tool (pcrlt-validator):
```console
user@host:~/some_path/target-repository/spdx-utils-main$ make forPcrltTool_0download-and-install-pcrlt-validator
```
To create PCRLT reports and a SPDX document template run:
```console
user@host:~/some_path/target-repository/spdx-utils-main$ make pcrltTool-all
```
The following files should be generated (if no error occurs):
* `~/some_path/target-repository/spdx-utils-output/pcrlt.txt` an overview report
* `~/some_path/target-repository/spdx-utils-output/pcrlt.json` the parsed pcrlt data in JSON format
* `~/some_path/target-repository/spdx-utils-output/pcrlt.csv` a csv export - one line per file.
* `~/some_path/target-repository/spdx-utils-output/pcrlt.spdx` a SPDX document
See pcrlt-validator[^pcrlt-validator] for further details on the file contents.

## Further Notes
The SPDX document `LICENSE.spdx` that is expected to be in `~/some_path/target-repository/LICENSE.spdx` can be generated and updated by using the pcrlt.spdx (or reuse.spdx) file generated before.  
To customize the config file `spdx-utils-main/config` (which contains the target repository path and some default values) a customized copy can be places in `spdx-utils-input/config`.
Note that the official REUSE tool[^reuse-tool] currently ignores[^reuse-ignore] some files (e.g. LICENSES/\*, LICENSE\*, \*.spdx, ./reuse/dep5) (on purpose). To be able to specify copyright and license information for all files a modified version is used (currently: https://gitlab.com/embeach/reuse-tool)  
Note that reuse.spdx and pcrlt.spdx may differ for the following reasons:
1. The reuse-tool uses single licenses for the `LicenseInfoInFile` values, while the pcrlt-validator uses license expressions. Both are allowed by SPDXv2.3 (expressions allow to quickly detect `OR` expressions without having to check the source files).
2. The reuse-tool parses many kinds of copyright and license annotations while the pcrlt-validator parses only `SPDX-FileCopyrightText:` and `SPDX-License-Identifier:` annotations in the target file and `Copyright:` and `License:` in the dep5 file.
3. The pcrlt-validator allows to specify multiple file headers for a single target file by using the CLI option `--multi-file-header-policy`. 
4. The pcrlt-validator uses the Git index to determine which files to process for parsing and SPDX document generation (i.e. files not in the Git index will be ignored)

### References
[^pcrlt-validator]: The PCRLT tool (pcrlt-validator) <https://gitlab.com/emb_std/pcrlt-validator>  
[^reuse-ignore]: The fsfe reuse tool ignore patterns are defined here (_IGNORE_DIR_PATTERNS,_IGNORE_FILE_PATTERNS): <https://github.com/fsfe/reuse-tool/blob/master/src/reuse/__init__.py>  
[^reuse-tool]: The REUSE tool (reuse/fsfe-reuse/reuse-tool): <https://git.fsfe.org/reuse/tool>, <https://github.com/fsfe/reuse-tool>  
[^spdx]: SPDXv2.3: <https://spdx.github.io/spdx-spec/>  
[^reuse]: REUSEv3.0: <https://reuse.software/spec/>  
[^pcrlt]: PCRLTv0.4: <https://gitlab.com/emb_std/pcrlt>
