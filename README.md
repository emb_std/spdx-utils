<!--
SPDX-FileCopyrightText: 2021-2022 embeach

SPDX-License-Identifier: MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later

Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/spdx-utils.git@master#README.md

Note-PCRLT:emb.7:specify-identity-name:embeach
Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/spdx-utils.git@master#README.md
Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding README.md

Note-PCRLT:MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"
-->
# SPDX Utils
The repository contains bootstrap instructions and scripts to use some tools to validate compliance with REUSE and PCLRT as well as to generate SPDX documents.  
Currently two tools are integrated.
1. fsfe reuse-tool <https://git.fsfe.org/reuse/tool> the following modified version is used: <https://gitlab.com/embeach/reuse-tool> (status: rather stable, features: check REUSE compliance, generate SPDX documents)
2. pcrlt-validator <https://gitlab.com/emb_std/pcrlt-validator> (status: rapid-prototype, features: check PCRLT compliance, generate SPDX documents, generate PCRLT reports)

The target repository (with REUSE or PCRLT annotations) must meet some requirements.  
Those can be found in [spdx-utils-main/README_SPDX_UTILS.md](spdx-utils-main/README_SPDX_UTILS.md) along with further details.  
<!-- For an example target repository, see <https://gitlab.com/emb_std/template-ca-lr>.  -->


## License
The repository is compliant with PCRLT[^pcrlt] and REUSE[^reuse] - i.e. copyright and license information are provided on a per-file basis. A summary is provided in LICENSE.spdx as SPDX[^spdx] document. Full license texts can be found in the LICENSES folder. 

The repository contains references to the following licenses: 0BSD, Apache-2.0, CC0-1.0, CC-BY-4.0, CC-BY-ND-4.0, GPL-3.0-or-later, MIT. Whereby most files have multiple license options, i.e. the licensee can choose.  
CC-BY-ND-4.0 is only used for normative files and therefore excluded from the concluded package license. The other files have at least one license option, that is "compatible" with the open source MIT license. Since CC0-1.0 seems to have no additional obligations when combined with e.g. MIT, the license option can be further simplified. The condensed form of the simplest package license options (SPDX PackageLicenseConcluded) is "MIT OR Apache-2.0 OR GPL-3.0-or-later". This means that the licensee can choose any license from the available options. E.g. "MIT". (See LICENSE.spdx for further explanations).  
(SPDX PackageLicenseDeclared is "MIT OR Apache-2.0 OR GPL-3.0-or-later" as well. Note: This is not an independent normative statement, but instead is the concluded package license based on the license information of the individual files.)

### MIT terms
"Software" (in the context of the MIT license in this repository) means, for a particular git version of this repository, all files or file parts that are licensed under MIT or that are multi-licensed with an MIT option. To determine which files and file parts are licensed under a particular license or that are multi-licensed with an option for that particular license, the REUSE and PCRLT annotations can be analyzed.  
"Copyright Holders" (in the context of the MIT license in this repository) are all copyright holders that hold copyright in parts of the "Software". The names can be determined by analyzing the REUSE and PCRLT annotations.  

### References
[^pcrlt-validator]: The PCRLT tool (pcrlt-validator) <https://gitlab.com/emb_std/pcrlt-validator>  
[^reuse-tool]: The REUSE tool (reuse/fsfe-reuse/reuse-tool): <https://git.fsfe.org/reuse/tool>, <https://github.com/fsfe/reuse-tool>  
[^spdx]: SPDXv2.3: <https://spdx.github.io/spdx-spec/>  
[^reuse]: REUSEv3.0: <https://reuse.software/spec/>  
[^pcrlt]: PCRLTv0.4: <https://gitlab.com/emb_std/pcrlt>  
